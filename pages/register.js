import { Meta } from "./seo/meta.js";
import { html } from "./ssg/basic.js";

export const head = () => {
  return Meta();
};

export default async () => {
  return html`<section class="relative py-20">
    <div class="container px-4 mx-auto">
      <div class="w-full lg:w-1/2 mb-12">
        <div class="lg:max-w-md">
          <div class="max-w-md mx-auto text-center py-10">
            <form action="#">
              <span
                class="inline-block mb-4 text-xs text-blue-400 font-semibold"
                >Sign Up</span
              >
              <h3 class="mb-12 text-3xl font-semibold font-heading">
                Create new account
              </h3>
              <div class="flex flex-wrap -mx-2">
                <div class="relative w-full lg:w-1/2 px-2 flex flex-wrap mb-6">
                  <input
                    class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                    type="text"
                    placeholder="John"
                  />
                  <span
                    class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                    >First name</span
                  >
                </div>
                <div class="relative w-full lg:w-1/2 px-2 flex flex-wrap mb-6">
                  <input
                    class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                    type="text"
                    placeholder="Smith"
                  />
                  <span
                    class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                    >Last name</span
                  >
                </div>
              </div>
              <div class="relative flex flex-wrap mb-6">
                <input
                  class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                  type="email"
                  placeholder="e.g hello@shuffle.dev"
                />
                <span
                  class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                  >Email address</span
                >
              </div>
              <div class="relative flex flex-wrap mb-6">
                <input
                  class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                  type="password"
                  placeholder="********"
                />
                <span
                  class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                  >Password</span
                >
              </div>
              <label class="inline-flex mb-10 text-left">
                <input class="mr-2" type="checkbox" name="terms" value="1" />
                <span class="-mt-1 inline-block text-sm text-gray-500"
                  >By signing up, you agree to our
                  <a class="text-red-400 hover:underline" href="#"
                    >Terms, Data Policy</a
                  >
                  and
                  <a class="text-red-400 hover:underline" href="#"
                    >Cookies Policy</a
                  >.</span
                >
              </label>
              <button
                class="w-full inline-block py-4 mb-4 text-sm text-white font-medium leading-normal bg-red-400 hover:bg-red-300 rounded transition duration-200"
              >
                Get Started
              </button>
              <p class="text-sm text-gray-500">
                <span>Already have an account?</span>
                <a class="text-red-400 hover:underline" href="#">Sign in</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div
      class="h-96 lg:h-auto lg:absolute top-0 right-0 bottom-0 lg:w-1/2 bg-no-repeat bg-contain bg-center"
      style="background-image: url('../assets/images/z-picture.png');"
    >
      <img
        class="hidden lg:block absolute bottom-0 right-0 mb-40 mr-40"
        src="../assets/icons/dots/yellow-dot-right-shield.svg"
        alt=""
      />
    </div>
  </section>`;
};
