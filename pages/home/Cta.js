import { html } from "../ssg/basic.js";
import "../hybrids/RedButton.js";

export const Cta = () => {
  return html`<section class="relative py-20">
    <img
      class="hidden lg:block absolute top-0 left-0 mt-20"
      src="../assets/icons/dots/blue-dot-left-bars.svg"
      alt=""
    /><img
      class="hidden lg:block absolute bottom-0 right-0 mb-40 mr-16"
      src="../assets/icons/dots/yellow-dot-right-shield.svg"
      alt=""
    />
    <div class="container px-4 mx-auto">
      <div class="max-w-2xl mx-auto mb-20 text-center">
        <span class="text-xs text-blue-400 font-semibold"
          >What's new at Shuffle</span
        >
        <h2 class="mt-8 mb-10 text-4xl font-semibold font-heading">
          Lorem ipsum dolor sit amet consectutar domor at elis
        </h2>
        <p class="mb-16 text-xl text-gray-500">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
          massa nibh, pulvinar vitae aliquet nec, accumsan aliquet orci.
        </p>
        <div class="flex items-center justify-center mb-10">
          <div class="mr-4 lg:mr-8">
            <span class="inline-block mr-1 lg:mr-4 text-grsay-500"
              >Lifetime license</span
            >
            <span
              class="inline-block px-3 py-1 text-xs bg-blue-50 text-blue-500 font-semibold rounded"
              >$150</span
            >
          </div>
          <div class="flex items-center p-1 bg-red-400 rounded-full">
            <button
              class="inline-block w-5 h-5 mr-1 bg-white rounded-full"
            ></button>
            <button
              class="inline-block w-5 h-5 bg-transparent rounded-full"
            ></button>
          </div>
          <div class="ml-4 lg:ml-8">
            <span class="inline-block mr-1 lg:mr-4 text-gray-500"
              >Monthly license</span
            >
            <span
              class="inline-block px-3 py-1 text-xs bg-blue-50 text-blue-500 font-semibold rounded"
              >$150</span
            >
          </div>
        </div>
      </div>
      <div
        class="max-w-4xl mx-auto flex flex-wrap border rounded-xl overflow-hidden"
      >
        <div class="w-full lg:w-1/2 mb-6 lg:mb-0">
          <div class="p-6 lg:p-12">
            <form action="">
              <span
                class="inline-block mb-4 text-sm font-semibold text-blue-400"
                >Sign Up</span
              >
              <h4 class="mb-10 text-3xl font-semibold font-heading">
                Finish your payment
              </h4>
              <div class="relative mb-6">
                <input
                  class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                  type="text"
                  placeholder="e.g hello@shuffle.dev"
                />
                <span
                  class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                  >Your email address</span
                >
              </div>
              <div class="relative mb-6">
                <input
                  class="relative mb-2 md:mb-0 w-full py-4 pl-4 text-sm border rounded"
                  type="password"
                  placeholder="*******"
                />
                <span
                  class="absolute top-0 left-0 ml-4 -mt-2 px-1 inline-block bg-white text-gray-500 text-xs"
                  >Password</span
                >
              </div>
              <div class="mb-10 text-left text-sm">
                <label>
                  <input type="checkbox" name="terms" value="1" />
                  <span class="text-sm text-gray-500 ml-1"
                    >By signing up, you agree to our
                    <a class="text-red-500 hover:underline" href="#"
                      >Terms, Data Policy</a
                    >
                    and
                    <a class="text-red-500 hover:underline" href="#"
                      >Cookies Policy</a
                    >.</span
                  >
                </label>
              </div>
              <red-button name="Get Started"></red-button>
            </form>
          </div>
        </div>
        <div class="w-full lg:w-1/2 h-full overflow-hidden">
          <img
            class="w-full max-h-128 lg:rounded-r-xl object-cover"
            src="https://images.unsplash.com/photo-1544717305-2782549b5136?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=900&amp;q=80"
            alt=""
          />
        </div>
      </div>
    </div>
  </section>`;
};
