import { html } from "../ssg/basic.js";
import { Nav } from "../components/Nav.js";
import "../hybrids/Link.js";
export const Header = () => {
  return html`<section class="relative bg-gray-900 pb-20">
    <img
      class="hidden lg:block absolute top-0 left-0 mt-40"
      src="../assets/icons/dots/yellow-dot-left-bars.svg"
      alt=""
    /><img
      class="hidden lg:block absolute bottom-0 right-0 mb-52"
      src="../assets/icons/dots/red-dot-right-shield.svg"
      alt=""
    />
    ${Nav()}
    <div class="relative container pt-12 px-4 mb-20 mx-auto text-center">
      <span class="text-blue-400 font-semibold">Kodeksy.online</span>
      <h2
        class="mt-8 mb-8 lg:mb-12 text-white text-4xl lg:text-6xl font-semibold"
      >
        API obsługujące Polskie kodeksy
      </h2>
      <p class="max-w-3xl mx-auto mb-8 lg:mb-12 text-white text-xl opacity-50">
        Podłącz się i zacznij budować nowe produkty w oparciu o polskie prawo
      </p>
      <custom-link
        name="Wypróbuj za darmo"
        href="#"
        color="bg-red-400"
        hover="bg-red-300"
      ></custom-link>
      <custom-link
        name="Kup teraz"
        href="#"
        color="bg-blue-400"
        hover="bg-blue-500"
      ></custom-link>
    </div>
    <div class="hidden navbar-menu relative z-50">
      <div class="navbar-backdrop fixed inset-0 bg-gray-800 opacity-25"></div>
      <nav
        class="fixed top-0 left-0 bottom-0 flex flex-col w-5/6 max-w-sm py-6 px-6 bg-white border-r overflow-y-auto"
      >
        <div class="flex items-center mb-8">
          <a class="mr-auto text-lg font-semibold leading-none" href="#"
            ><img
              class="h-7"
              src="../assets/logo/logo-zeus-red.svg"
              alt=""
              width="auto"
          /></a>
          <button class="navbar-close">
            <svg
              class="h-6 w-6 text-gray-500 cursor-pointer hover:text-gray-500"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
              ></path>
            </svg>
          </button>
        </div>
        <div>
          <ul>
            <li class="mb-1">
              <a
                class="block p-4 text-sm font-medium text-gray-900 hover:bg-gray-50 rounded"
                href="#"
                >About</a
              >
            </li>
            <li class="mb-1">
              <a
                class="block p-4 text-sm font-medium text-gray-900 hover:bg-gray-50 rounded"
                href="#"
                >Company</a
              >
            </li>
            <li class="mb-1">
              <a
                class="block p-4 text-sm font-medium text-gray-900 hover:bg-gray-50 rounded"
                href="#"
                >Services</a
              >
            </li>
            <li class="mb-1">
              <a
                class="block p-4 text-sm font-medium text-gray-900 hover:bg-gray-50 rounded"
                href="#"
                >Testimonials</a
              >
            </li>
          </ul>
        </div>
        <div class="mt-auto">
          <div class="pt-6">
            <a
              class="block py-3 text-center text-sm leading-normal rounded bg-red-50 hover:bg-red-200 text-red-500 font-semibold transition duration-200"
              href="#"
              >Zaloguj się</a
            >
          </div>
          <p class="mt-6 mb-4 text-sm text-center text-gray-500">
            <span>© 2021 All rights reserved.</span>
          </p>
        </div>
      </nav>
    </div>
  </section>`;
};
