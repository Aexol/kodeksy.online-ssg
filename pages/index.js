import { Footer } from "./components/Footer.js";
import { Cta } from "./home/Cta.js";
import { Header } from "./home/Header.js";
import { Meta } from "./seo/meta.js";
import { html } from "./ssg/basic.js";

export const head = () => {
  return Meta();
};

export default () => {
  return html`${Header()}${Cta()}${Footer()}`;
};
