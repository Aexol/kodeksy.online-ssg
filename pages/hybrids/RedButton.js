import { html, define } from "https://cdn.skypack.dev/hybrids";
const RedButton = {
  name: "",
  content: ({ name }) =>
    html` <button
      class="block w-full px-6 py-4 text-sm text-white font-medium leading-normal bg-red-400 hover:bg-red-300 rounded transition duration-200"
    >
      ${name}
    </button>`,
};

define("red-button", RedButton);
