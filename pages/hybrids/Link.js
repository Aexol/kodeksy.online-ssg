import { html, define } from "https://cdn.skypack.dev/hybrids";

const Link = {
  href: "#",
  name: "",
  color: "bg-red-400",
  hover: "bg-red-300",
  content: ({ href, name, color, hover }) =>
    html` <a
      class="inline-block w-full md:w-auto mb-2 md:mb-0 px-8 py-4 mr-2 text-white text-sm font-medium leading-normal ${color} hover:${hover} rounded transition duration-200"
      href=${href}
      >${name}</a
    >`,
};

define("custom-link", Link);
