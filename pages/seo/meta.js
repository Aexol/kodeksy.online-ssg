import { html } from "../ssg/basic.js";

export const Meta = () => {
  return html`<meta
      name="viewport"
      content="initial-scale=1.0, width=device-width"
    /><meta name="description" content="Api kodeksy" />
    <title>Kodeksy.online</title
    ><link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
    <link
      rel="stylesheet"
      type="text/css"
      media="screen"
      href="./tailwind.css"
    />`;
};
