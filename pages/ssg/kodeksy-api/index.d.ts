type ZEUS_INTERFACES = never
type ZEUS_UNIONS = never

export type ValueTypes = {
    ["AdminMutation"]: AliasType<{
addConsumer?: [{	consumerInput:ValueTypes["ConsumerInput"]},true],
createArticle?: [{	article:ValueTypes["CreateArticle"]},true],
createCodex?: [{	codex:ValueTypes["CreateCodex"]},true],
removeArticle?: [{	/** codex id */
	_id?:ValueTypes["ObjectId"]},true],
removeCodex?: [{	/** codex name */
	name?:string},true],
updateArticle?: [{	article:ValueTypes["UpdateArticle"]},true],
updateCodex?: [{	name:string},true],
	updateCodexes?:true,
updateConsumer?: [{	consumerInput:ValueTypes["ConsumerInput"]},true],
		__typename?: true
}>;
	/** Query the access of the user to the app */
["AccessQuery"]: AliasType<{
	/** Return roles of the user */
	roles?:true,
		__typename?: true
}>;
	/** All roles available around the system */
["Role"]:Role;
	["UpdateArticle"]: {
	nr?:string,
	content?:string,
	codex?:ValueTypes["ObjectId"],
	tags?:string[],
	_id:ValueTypes["ObjectId"]
};
	["AdminQuery"]: AliasType<{
	health?:true,
		__typename?: true
}>;
	["LoggedInData"]: AliasType<{
	token?:true,
		__typename?: true
}>;
	["ConsumerInput"]: {
	email:string,
	ownerUsername:string,
	allowedCodexes:string[]
};
	["ConsumerMutation"]: AliasType<{
tagArticle?: [{	value:ValueTypes["TagArticle"]},true],
		__typename?: true
}>;
	["TagArticle"]: {
	codex:string,
	tag:string,
	article_number:string
};
	["ArticleVersion"]: AliasType<{
	/** article */
	article?:ValueTypes["Article"],
	/** The date when article was updated by legislator */
	updatedAt?:true,
		__typename?: true
}>;
	["SearchResponse"]: AliasType<{
	articles?:ValueTypes["Article"],
	tags?:ValueTypes["Tag"],
		__typename?: true
}>;
	["CreateCodex"]: {
	/** Pass Articles to create them together with codex */
	articles?:ValueTypes["CreateArticle"][],
	link:string,
	/** Full name of the codex */
	name:string
};
	/** All mutations of users system */
["UserMutation"]: AliasType<{
changePassword?: [{	changePassword:ValueTypes["ChangePassword"]},ValueTypes["LoggedInData"]],
forgotPassword?: [{	username:string},true],
makeAdmin?: [{	/** username of admin user<br> */
	username:string},true],
register?: [{	user:ValueTypes["UserBasicData"]},ValueTypes["LoggedInData"]],
resetPassword?: [{	reset:ValueTypes["ResetPassword"]},true],
		__typename?: true
}>;
	["Query"]: AliasType<{
	access?:ValueTypes["AccessQuery"],
	admin?:ValueTypes["AdminQuery"],
	consumerQuery?:ValueTypes["ConsumerQuery"],
	/** Queries from users system */
	users?:ValueTypes["UserQuery"],
		__typename?: true
}>;
	/** All queries of users system */
["UserQuery"]: AliasType<{
	/** Check if logged in user is admin<br> */
	isAdmin?:true,
	/** Check if there is admin already */
	isAdminClaimPossible?:true,
login?: [{	user:ValueTypes["UserBasicData"]},ValueTypes["LoggedInData"]],
		__typename?: true
}>;
	["ArticleInput"]: {
	articleNumber:string,
	codexName:string
};
	["Mutation"]: AliasType<{
	admin?:ValueTypes["AdminMutation"],
	/** Mutations for users that bought access */
	consumer?:ValueTypes["ConsumerMutation"],
	/** Mutations of the users system */
	users?:ValueTypes["UserMutation"],
		__typename?: true
}>;
	["CreateArticle"]: {
	/** Content of the article */
	content:string,
	codex:ValueTypes["ObjectId"],
	tags?:string[],
	/** Number of article in codex */
	nr:string
};
	["ChangePassword"]: {
	newPassword:string,
	password:string
};
	["ConsumerQuery"]: AliasType<{
article?: [{	articleInput:ValueTypes["ArticleInput"]},ValueTypes["Article"]],
articles?: [{	codex:string},ValueTypes["Article"]],
codexById?: [{	/** codex id */
	_id?:ValueTypes["ObjectId"]},ValueTypes["Codex"]],
	/** List of all available codexes */
	codexes?:ValueTypes["Codex"],
	/** Search articles and tags */
	search?:ValueTypes["SearchResponse"],
tagByName?: [{	/** name of the tag */
	name?:string},ValueTypes["Tag"]],
	/** Queries from users system */
	users?:ValueTypes["UserQuery"],
		__typename?: true
}>;
	/** Codex containing articles */
["Codex"]: AliasType<{
	_id?:true,
	/** All articles in this document */
	articles?:true,
	/** Last article inside codex modification time */
	lastModified?:true,
	/** Link to codex document. */
	link?:true,
	/** Full name of the codex */
	name?:true,
		__typename?: true
}>;
	["ObjectId"]:unknown;
	["Date"]:unknown;
	/** Article in Document */
["Article"]: AliasType<{
	_id?:true,
	/** Codex containing this article */
	codex?:true,
	/** Content of the article */
	content?:true,
	/** Number of article in codex */
	nr?:true,
	/** Tags describing this article */
	tags?:true,
	/** all version of the article */
	versions?:ValueTypes["ArticleVersion"],
		__typename?: true
}>;
	["UserBasicData"]: {
	username:string,
	password:string
};
	/** Tag can be placed on the article. Article can contain multiple tags and Tag can be used in multiple articles. */
["Tag"]: AliasType<{
	/** All articles having this tag */
	articles?:true,
	/** Tag content and name */
	name?:true,
		__typename?: true
}>;
	/** Reset password details */
["ResetPassword"]: {
	/** token received from email */
	token:string,
	/** New password for the user */
	newPassword:string
}
  }

export type ModelTypes = {
    ["AdminMutation"]: {
		addConsumer?:boolean,
	createArticle?:boolean,
	createCodex?:boolean,
	removeArticle?:boolean,
	/** Remove codex */
	removeCodex?:boolean,
	updateArticle?:boolean,
	updateCodex?:boolean,
	updateCodexes?:boolean,
	updateConsumer?:boolean
};
	/** Query the access of the user to the app */
["AccessQuery"]: {
		/** Return roles of the user */
	roles:ModelTypes["Role"][]
};
	/** All roles available around the system */
["Role"]: GraphQLTypes["Role"];
	["UpdateArticle"]: GraphQLTypes["UpdateArticle"];
	["AdminQuery"]: {
		health?:number
};
	["LoggedInData"]: {
		token?:string
};
	["ConsumerInput"]: GraphQLTypes["ConsumerInput"];
	["ConsumerMutation"]: {
		tagArticle?:boolean
};
	["TagArticle"]: GraphQLTypes["TagArticle"];
	["ArticleVersion"]: {
		/** article */
	article?:ModelTypes["Article"],
	/** The date when article was updated by legislator */
	updatedAt:ModelTypes["Date"]
};
	["SearchResponse"]: {
		articles?:ModelTypes["Article"][],
	tags?:ModelTypes["Tag"][]
};
	["CreateCodex"]: GraphQLTypes["CreateCodex"];
	/** All mutations of users system */
["UserMutation"]: {
		changePassword:ModelTypes["LoggedInData"],
	forgotPassword?:boolean,
	/** Make user a superadmin on a first call. Then you need to be an admin to call this */
	makeAdmin?:boolean,
	/** Register a new user<br> */
	register?:ModelTypes["LoggedInData"],
	resetPassword?:boolean
};
	["Query"]: {
		access?:ModelTypes["AccessQuery"],
	admin?:ModelTypes["AdminQuery"],
	consumerQuery?:ModelTypes["ConsumerQuery"],
	/** Queries from users system */
	users?:ModelTypes["UserQuery"]
};
	/** All queries of users system */
["UserQuery"]: {
		/** Check if logged in user is admin<br> */
	isAdmin?:boolean,
	/** Check if there is admin already */
	isAdminClaimPossible?:boolean,
	/** Log user in */
	login?:ModelTypes["LoggedInData"]
};
	["ArticleInput"]: GraphQLTypes["ArticleInput"];
	["Mutation"]: {
		admin?:ModelTypes["AdminMutation"],
	/** Mutations for users that bought access */
	consumer?:ModelTypes["ConsumerMutation"],
	/** Mutations of the users system */
	users?:ModelTypes["UserMutation"]
};
	["CreateArticle"]: GraphQLTypes["CreateArticle"];
	["ChangePassword"]: GraphQLTypes["ChangePassword"];
	["ConsumerQuery"]: {
		article:ModelTypes["Article"],
	articles:ModelTypes["Article"][],
	codexById:ModelTypes["Codex"],
	/** List of all available codexes */
	codexes:ModelTypes["Codex"][],
	/** Search articles and tags */
	search?:ModelTypes["SearchResponse"],
	tagByName:ModelTypes["Tag"],
	/** Queries from users system */
	users?:ModelTypes["UserQuery"]
};
	/** Codex containing articles */
["Codex"]: {
		_id:ModelTypes["ObjectId"],
	/** All articles in this document */
	articles:ModelTypes["ObjectId"][],
	/** Last article inside codex modification time */
	lastModified:ModelTypes["Date"],
	/** Link to codex document. */
	link:string,
	/** Full name of the codex */
	name:string
};
	["ObjectId"]:any;
	["Date"]:any;
	/** Article in Document */
["Article"]: {
		_id:ModelTypes["ObjectId"],
	/** Codex containing this article */
	codex:ModelTypes["ObjectId"],
	/** Content of the article */
	content:string,
	/** Number of article in codex */
	nr:string,
	/** Tags describing this article */
	tags?:string[],
	/** all version of the article */
	versions:ModelTypes["ArticleVersion"][]
};
	["UserBasicData"]: GraphQLTypes["UserBasicData"];
	/** Tag can be placed on the article. Article can contain multiple tags and Tag can be used in multiple articles. */
["Tag"]: {
		/** All articles having this tag */
	articles:ModelTypes["ObjectId"][],
	/** Tag content and name */
	name:string
};
	/** Reset password details */
["ResetPassword"]: GraphQLTypes["ResetPassword"]
    }

export type GraphQLTypes = {
    ["AdminMutation"]: {
	__typename: "AdminMutation",
	addConsumer?: boolean,
	createArticle?: boolean,
	createCodex?: boolean,
	removeArticle?: boolean,
	/** Remove codex */
	removeCodex?: boolean,
	updateArticle?: boolean,
	updateCodex?: boolean,
	updateCodexes?: boolean,
	updateConsumer?: boolean
};
	/** Query the access of the user to the app */
["AccessQuery"]: {
	__typename: "AccessQuery",
	/** Return roles of the user */
	roles: Array<GraphQLTypes["Role"]>
};
	/** All roles available around the system */
["Role"]: Role;
	["UpdateArticle"]: {
		nr?: string,
	content?: string,
	codex?: GraphQLTypes["ObjectId"],
	tags?: Array<string>,
	_id: GraphQLTypes["ObjectId"]
};
	["AdminQuery"]: {
	__typename: "AdminQuery",
	health?: number
};
	["LoggedInData"]: {
	__typename: "LoggedInData",
	token?: string
};
	["ConsumerInput"]: {
		email: string,
	ownerUsername: string,
	allowedCodexes: Array<string>
};
	["ConsumerMutation"]: {
	__typename: "ConsumerMutation",
	tagArticle?: boolean
};
	["TagArticle"]: {
		codex: string,
	tag: string,
	article_number: string
};
	["ArticleVersion"]: {
	__typename: "ArticleVersion",
	/** article */
	article?: GraphQLTypes["Article"],
	/** The date when article was updated by legislator */
	updatedAt: GraphQLTypes["Date"]
};
	["SearchResponse"]: {
	__typename: "SearchResponse",
	articles?: Array<GraphQLTypes["Article"]>,
	tags?: Array<GraphQLTypes["Tag"]>
};
	["CreateCodex"]: {
		/** Pass Articles to create them together with codex */
	articles?: Array<GraphQLTypes["CreateArticle"]>,
	link: string,
	/** Full name of the codex */
	name: string
};
	/** All mutations of users system */
["UserMutation"]: {
	__typename: "UserMutation",
	changePassword: GraphQLTypes["LoggedInData"],
	forgotPassword?: boolean,
	/** Make user a superadmin on a first call. Then you need to be an admin to call this */
	makeAdmin?: boolean,
	/** Register a new user<br> */
	register?: GraphQLTypes["LoggedInData"],
	resetPassword?: boolean
};
	["Query"]: {
	__typename: "Query",
	access?: GraphQLTypes["AccessQuery"],
	admin?: GraphQLTypes["AdminQuery"],
	consumerQuery?: GraphQLTypes["ConsumerQuery"],
	/** Queries from users system */
	users?: GraphQLTypes["UserQuery"]
};
	/** All queries of users system */
["UserQuery"]: {
	__typename: "UserQuery",
	/** Check if logged in user is admin<br> */
	isAdmin?: boolean,
	/** Check if there is admin already */
	isAdminClaimPossible?: boolean,
	/** Log user in */
	login?: GraphQLTypes["LoggedInData"]
};
	["ArticleInput"]: {
		articleNumber: string,
	codexName: string
};
	["Mutation"]: {
	__typename: "Mutation",
	admin?: GraphQLTypes["AdminMutation"],
	/** Mutations for users that bought access */
	consumer?: GraphQLTypes["ConsumerMutation"],
	/** Mutations of the users system */
	users?: GraphQLTypes["UserMutation"]
};
	["CreateArticle"]: {
		/** Content of the article */
	content: string,
	codex: GraphQLTypes["ObjectId"],
	tags?: Array<string>,
	/** Number of article in codex */
	nr: string
};
	["ChangePassword"]: {
		newPassword: string,
	password: string
};
	["ConsumerQuery"]: {
	__typename: "ConsumerQuery",
	article: GraphQLTypes["Article"],
	articles: Array<GraphQLTypes["Article"]>,
	codexById: GraphQLTypes["Codex"],
	/** List of all available codexes */
	codexes: Array<GraphQLTypes["Codex"]>,
	/** Search articles and tags */
	search?: GraphQLTypes["SearchResponse"],
	tagByName: GraphQLTypes["Tag"],
	/** Queries from users system */
	users?: GraphQLTypes["UserQuery"]
};
	/** Codex containing articles */
["Codex"]: {
	__typename: "Codex",
	_id: GraphQLTypes["ObjectId"],
	/** All articles in this document */
	articles: Array<GraphQLTypes["ObjectId"]>,
	/** Last article inside codex modification time */
	lastModified: GraphQLTypes["Date"],
	/** Link to codex document. */
	link: string,
	/** Full name of the codex */
	name: string
};
	["ObjectId"]:any;
	["Date"]:any;
	/** Article in Document */
["Article"]: {
	__typename: "Article",
	_id: GraphQLTypes["ObjectId"],
	/** Codex containing this article */
	codex: GraphQLTypes["ObjectId"],
	/** Content of the article */
	content: string,
	/** Number of article in codex */
	nr: string,
	/** Tags describing this article */
	tags?: Array<string>,
	/** all version of the article */
	versions: Array<GraphQLTypes["ArticleVersion"]>
};
	["UserBasicData"]: {
		username: string,
	password: string
};
	/** Tag can be placed on the article. Article can contain multiple tags and Tag can be used in multiple articles. */
["Tag"]: {
	__typename: "Tag",
	/** All articles having this tag */
	articles: Array<GraphQLTypes["ObjectId"]>,
	/** Tag content and name */
	name: string
};
	/** Reset password details */
["ResetPassword"]: {
		/** token received from email */
	token: string,
	/** New password for the user */
	newPassword: string
}
    }
/** All roles available around the system */
export enum Role {
	CONSUMER = "CONSUMER",
	ADMIN = "ADMIN"
}


export type UnwrapPromise<T> = T extends Promise<infer R> ? R : T;
export type ZeusState<T extends (...args: any[]) => Promise<any>> = NonNullable<
  UnwrapPromise<ReturnType<T>>
>;
export type ZeusHook<
  T extends (
    ...args: any[]
  ) => Record<string, (...args: any[]) => Promise<any>>,
  N extends keyof ReturnType<T>
> = ZeusState<ReturnType<T>[N]>;

type WithTypeNameValue<T> = T & {
  __typename?: true;
};
type AliasType<T> = WithTypeNameValue<T> & {
  __alias?: Record<string, WithTypeNameValue<T>>;
};
export interface GraphQLResponse {
  data?: Record<string, any>;
  errors?: Array<{
    message: string;
  }>;
}
type DeepAnify<T> = {
  [P in keyof T]?: any;
};
type IsPayLoad<T> = T extends [any, infer PayLoad] ? PayLoad : T;
type IsArray<T, U> = T extends Array<infer R> ? InputType<R, U>[] : InputType<T, U>;
type FlattenArray<T> = T extends Array<infer R> ? R : T;

type NotUnionTypes<SRC extends DeepAnify<DST>, DST> = {
  [P in keyof DST]: SRC[P] extends '__union' & infer R ? never : P;
}[keyof DST];

type ExtractUnions<SRC extends DeepAnify<DST>, DST> = {
  [P in keyof SRC]: SRC[P] extends '__union' & infer R
    ? P extends keyof DST
      ? IsArray<R, DST[P] & { __typename: true }>
      : {}
    : never;
}[keyof SRC];

type IsInterfaced<SRC extends DeepAnify<DST>, DST> = FlattenArray<SRC> extends ZEUS_INTERFACES | ZEUS_UNIONS
  ? ExtractUnions<SRC, DST> &
      {
        [P in keyof Omit<Pick<SRC, NotUnionTypes<SRC, DST>>, '__typename'>]: DST[P] extends true
          ? SRC[P]
          : IsArray<SRC[P], DST[P]>;
      }
  : {
      [P in keyof Pick<SRC, keyof DST>]: DST[P] extends true ? SRC[P] : IsArray<SRC[P], DST[P]>;
    };



export type MapType<SRC, DST> = SRC extends DeepAnify<DST> ? IsInterfaced<SRC, DST> : never;
type InputType<SRC, DST> = IsPayLoad<DST> extends { __alias: infer R }
  ? {
      [P in keyof R]: MapType<SRC, R[P]>;
    } &
      MapType<SRC, Omit<IsPayLoad<DST>, '__alias'>>
  : MapType<SRC, IsPayLoad<DST>>;
type Func<P extends any[], R> = (...args: P) => R;
type AnyFunc = Func<any, any>;
export type ArgsType<F extends AnyFunc> = F extends Func<infer P, any> ? P : never;
export type OperationToGraphQL<V, T> = <Z extends V>(o: Z | V, variables?: Record<string, any>) => Promise<InputType<T, Z>>;
export type SubscriptionToGraphQL<V, T> = <Z extends V>(
  o: Z | V,
  variables?: Record<string, any>,
) => {
  ws: WebSocket;
  on: (fn: (args: InputType<T, Z>) => void) => void;
  off: (e: { data?: InputType<T, Z>; code?: number; reason?: string; message?: string }) => void;
  error: (e: { data?: InputType<T, Z>; message?: string }) => void;
  open: () => void;
};
export type CastToGraphQL<V, T> = (resultOfYourQuery: any) => <Z extends V>(o: Z | V) => InputType<T, Z>;
export type SelectionFunction<V> = <T>(t: T | V) => T;
export type fetchOptions = ArgsType<typeof fetch>;
type websocketOptions = typeof WebSocket extends new (
  ...args: infer R
) => WebSocket
  ? R
  : never;
export type chainOptions =
  | [fetchOptions[0], fetchOptions[1] & {websocket?: websocketOptions}]
  | [fetchOptions[0]];
export type FetchFunction = (
  query: string,
  variables?: Record<string, any>,
) => Promise<any>;
export type SubscriptionFunction = (
  query: string,
  variables?: Record<string, any>,
) => void;
type NotUndefined<T> = T extends undefined ? never : T;
export type ResolverType<F> = NotUndefined<F extends [infer ARGS, any] ? ARGS : undefined>;

export declare function Thunder(
  fn: FetchFunction,
  subscriptionFn: SubscriptionFunction
):{
  query: OperationToGraphQL<ValueTypes["Query"],GraphQLTypes["Query"]>,mutation: OperationToGraphQL<ValueTypes["Mutation"],GraphQLTypes["Mutation"]>
}

export declare function Chain(
  ...options: chainOptions
):{
  query: OperationToGraphQL<ValueTypes["Query"],GraphQLTypes["Query"]>,mutation: OperationToGraphQL<ValueTypes["Mutation"],GraphQLTypes["Mutation"]>
}

export declare const Zeus: {
  query: (o: ValueTypes["Query"]) => string,mutation: (o: ValueTypes["Mutation"]) => string
}

export declare const Cast: {
  query: CastToGraphQL<
  ValueTypes["Query"],
  GraphQLTypes["Query"]
>,mutation: CastToGraphQL<
  ValueTypes["Mutation"],
  GraphQLTypes["Mutation"]
>
}

export declare const Selectors: {
  query: SelectionFunction<ValueTypes["Query"]>,mutation: SelectionFunction<ValueTypes["Mutation"]>
}

export declare const resolverFor : <
  T extends keyof ValueTypes,
  Z extends keyof ValueTypes[T],
  Y extends (
    args: Required<ValueTypes[T]>[Z] extends [infer Input, any] ? Input : any,
    source: any,
  ) => Z extends keyof ModelTypes[T] ? ModelTypes[T][Z] | Promise<ModelTypes[T][Z]> : any
>(
  type: T,
  field: Z,
  fn: Y,
) => (args?:any, source?:any) => void;

export declare const Gql: ReturnType<typeof Chain>
