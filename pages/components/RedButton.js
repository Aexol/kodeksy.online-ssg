import { html } from "../ssg/basic.js";

export const RedButton = (name) => {
  return html` <button
    class="block w-full px-6 py-4 text-sm text-white font-medium leading-normal bg-red-400 hover:bg-red-300 rounded transition duration-200"
  >
    ${name}
  </button>`;
};
