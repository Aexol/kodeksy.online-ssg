module.exports = {
  ...require("./tailwind.config"),
  purge: {
    enabled: true,
    content: ["./out/**/*.html", "./out/**/*.js"],
  },
};
